import axios from "axios";

const getUri = (uri) => {
  return axios
    .create({
      timeout: 60000,
    })
    .get(uri);
};

export { getUri };
