import { createRouter, createWebHistory } from "vue-router";
import PokedexView from "../views/Pokedex/PokedexView.vue";
import PokemonDetail from "../views/PokemonDetail/PokemonDetail.vue";

const routes = [
  {
    path: "/",
    name: "pokedex",
    component: PokedexView,
  },
  {
    path: "/:name",
    name: "pokemonDetail",
    component: PokemonDetail,
  },
  {
    path: "/favourite",
    name: "favourite",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(
        /* webpackChunkName: "about" */ "../views/Favourite/FavouriteView.vue"
      ),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
