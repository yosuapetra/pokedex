import { defineStore } from "pinia";
import { getUri } from "@/api/PokemonApi.js";

export const usePokemonDetailStore = defineStore("pokemonDetail", {
  state: () => {
    return { pokemonDetailList: [], pokemonAbilityList: [] };
  },
  actions: {
    async fetchPokemonDetail(name) {
      if (this.pokemonDetailList.find((x) => x.name == name)) return;

      // if not cached then fetch
      const result = await getUri(`https://pokeapi.co/api/v2/pokemon/${name}`);
      const species = await getUri(result.data.species.url);
      result.data.species = species.data;
      this.pokemonDetailList.push(result.data);
    },
  },
  getters: {
    getPokemonDetail: function (state) {
      return (name) => { 
        return state.pokemonDetailList.find((x) => x.name === name);
      };
    },
  },
});
