import { defineStore } from "pinia";

export const useNavigationStore = defineStore("navigation", {
  state: () => {
    return { isOpen: false, slide: false };
  },
  actions: {
    toggleSidebar() {
      this.isOpen = !this.isOpen;
    },
    toggleSlide() {
      this.slide = !this.slide;
    },
  },
});
