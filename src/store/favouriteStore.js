import { defineStore } from "pinia";

export const useFavouriteStore = defineStore("favourite", {
  state: () => {
    return { favouriteList: [] };
  },
  actions: {
    toggleFavourite(pokemon) {
      if (this.favouriteList.some((x) => x === pokemon)) {
        this.favouriteList = this.favouriteList.filter((x) => x != pokemon);
      } else this.favouriteList.push(pokemon);
    },
  },
  getters: {
    getFavouriteList: function (state) {
      return state.favouriteList;
    },
  },
  persist: {
    enabled: true,
    strategies: [
      {
        key: "pokedex",
        storage: localStorage,
      },
    ],
  },
});
