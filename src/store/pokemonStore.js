import { defineStore } from "pinia";
import { getUri } from "@/api/PokemonApi.js";
import { useFilterStore } from "./filterStore";
import { usePokemonDetailStore } from "./pokemonDetailStore";

export const usePokemonStore = defineStore("pokemon", {
  state: () => {
    return {
      pokemonList: [],
      filteredList: [],
      nextUrl: "",
      isLoading: false,
    };
  },
  actions: {
    async setFilteredList(listFilteredPokemon) {
      this.filteredList = [];
      listFilteredPokemon.forEach(async (x) => {
        await usePokemonDetailStore().fetchPokemonDetail(x.name);
        this.filteredList.push(x);
      });
    },
    async fetchNextPage(loadMoreState) {
      if (this.pokemonList.length == 0)
        this.nextUrl = "https://pokeapi.co/api/v2/pokemon/?limit=20";

      const result = await getUri(this.nextUrl);
      this.nextUrl = result.data.next;
      result.data.results.forEach(async (x) => {
        await usePokemonDetailStore().fetchPokemonDetail(x.name);
        this.pokemonList.push(x);
      });
      loadMoreState.loaded();
    },
  },
  getters: {
    getPokemonList: function (state) {
      const filterStore = useFilterStore();
      if (filterStore.selected) {
        return state.filteredList;
      } else return state.pokemonList;
    },
  },
});
