import { defineStore } from "pinia";
import { getUri } from "@/api/PokemonApi.js";
import { usePokemonStore } from "./pokemonStore";

export const useFilterStore = defineStore("filter", {
  state: () => {
    return { filterList: [], selected: null };
  },
  actions: {
    async fetchFilterList() {
      if (this.filterList.length > 0) return;

      const url = "https://pokeapi.co/api/v2/type/";
      const result = await getUri(url);
      this.filterList = result.data.results;
    },
    async resetFilter() {
      this.selected = null;
      // const pokemonStore = usePokemonStore();
    },
    async setFilter(filter) {
      this.selected = filter;
      const url = "https://pokeapi.co/api/v2/type/" + filter;
      const result = await getUri(url);
      const pokemonStore = usePokemonStore();
      await pokemonStore.setFilteredList(
        result.data.pokemon.map((x) => x.pokemon)
      );
    },
  },
});
