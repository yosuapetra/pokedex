import { defineStore } from "pinia";

export const useModalStore = defineStore("modal", {
  state: () => {
    return { showModal: false, modalType: "" };
  },
  actions: {
    async toggleModal(modalType) {
      this.modalType = modalType;
      this.showModal = !this.showModal;
    },
  },
});
