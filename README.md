# pokedex dummy that consume pokeAPI

## Install Project depedency for local environment user

```
yarn install
```

### Build Project for local environment user

```
yarn serve
```

### Compiles and minifies for production

```
yarn build
```

### Lints and fixes files

```
yarn lint
```

### Build dockerized version for cloud deployment

```
sh build.sh
```

### Host dockerized version

```
sh start.sh
```
